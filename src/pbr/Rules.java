package pbr;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

public class Rules {
	List<Match> matches;

	int small_to_large = 0;
	int large_to_small = 0;
	int undefined_direction = 0;
	int total = 0;

	int[] specialPositionsOut;
	int[] specialPositionsIn;

	int moves_equal_dice_number;
	int moves_not_equal_dice_number;
	int moves_double_dice_number;
	int no_move_dice_number;
	int different_number;
	int same_dice_on_double_move;
	int piece_to_column_25;

	int dice_move_eq_nr_index_moved;
	int dice_out_moved_nr;

	public void determine_rules(List<Match> matches) {
		this.matches = matches;
		determineMovingDirection();
		determineSpecialPositions();
		determineDiceCorelation();
	}

	// dice to move correlation
	private void determineDiceCorelation() {
		System.out.println("========== DICE TO MOVE CORRELATION =========");
		moves_equal_dice_number = 0;
		moves_not_equal_dice_number = 0;
		moves_double_dice_number = 0;
		no_move_dice_number = 0;
		different_number = 0;
		same_dice_on_double_move = 0;
		piece_to_column_25 = 0;
		dice_move_eq_nr_index_moved = 0;
		dice_out_moved_nr = 0;

		for (Match m : matches) {
			checkDiceToNumMoves(m.states(0), m.getDices(0));
			checkDiceToMove(m.states(0), m.getDices(0));
		}

		for (Match m : matches) {
			checkDiceToNumMoves(m.states(1), m.getDices(1));
			checkDiceToMove(m.states(1), m.getDices(1));
		}
		printDiceToMoveCorrelation(2);

		if (no_move_dice_number > 0) {
			determineNoMoveRule();
		}

		System.out.println("Rule: two dices are the same, then the number of moves is double. Probability: "
				+ String.valueOf(same_dice_on_double_move * 100.0 / moves_double_dice_number) + "%");
		System.out.println();
//		System.out.println("Normal moving pieces: " + String.valueOf(moves_equal_dice_number - dice_out_moved_nr));
		System.out.println("Pieces moved the same number as the dice value: "
				+ String.valueOf(dice_move_eq_nr_index_moved * 100.0 / (moves_equal_dice_number - dice_out_moved_nr)) + "%");
		System.out.println("Unknown dice number to move corelation: "
				+ String.valueOf((moves_equal_dice_number - dice_out_moved_nr - dice_move_eq_nr_index_moved) * 100.0
						/ (moves_equal_dice_number - dice_out_moved_nr)) + "%");
	}

	private void checkDiceToMove(HashMap<Integer, ArrayList<List<Integer>>> states, HashMap<Integer, List<Integer>> dices) {
		List<Integer> prev_move = null;
		for (Integer key : states.keySet()) {
			ArrayList<List<Integer>> state = states.get(key);
			if (key == 0) {
				prev_move = states.get(key).get(0);
				continue;
			}
			ArrayList<List<Integer>> moves = states.get(key);
			if (dices.get(key).size() == state.size()) {

				if (prev_move != null) {
					boolean moved_out = false;
					int[] marked_dices = new int[dices.get(key).size()];
					for (List<Integer> move : state) {

						int index_minus = 0, index_plus = 0;
						// determine changed indexes
						for (int i = 1; i < move.size() - 1; i++) {
							if (prev_move.get(i) != move.get(i)) {
								if (prev_move.get(i) > move.get(i)) {
									index_minus = i;
								} else {
									index_plus = i;
								}
							}
						}
						// piece has moved
						if (index_minus != 0 && index_plus != 0) {
							for (int i = 0; i < dices.get(key).size(); i++) {
								if (dices.get(key).get(i) == Math.abs(index_minus - index_plus)) {
									marked_dices[i] = 1;
									break;
								}
							}

						} else if (index_plus == 0 && index_minus != 0 && move.get(0) != prev_move.get(0)) {
							moved_out = true;
						}
						prev_move = move;
					}
					boolean dice_good = true;
					for (int i = 0; i < marked_dices.length; i++) {
						if (marked_dices[i] != 1) {
							dice_good = false;
							break;
						}
					}
					if (dice_good) {
						dice_move_eq_nr_index_moved++;
					}

					if (moved_out) {
						dice_out_moved_nr++;
					}

				}
			}
			if (moves.size() != 0) {
				prev_move = moves.get(moves.size() - 1);
			}

		}

	}

	private void printDiceToMoveCorrelation(int player) {
		System.out.println("Total moves: " + String.valueOf(moves_equal_dice_number + moves_not_equal_dice_number));
		System.out.println("Move equals dice number: "
				+ String.valueOf(moves_equal_dice_number * 100 / (moves_equal_dice_number + moves_not_equal_dice_number)) + "%");
		// System.out.println("Move not equals dice number: " +
		// String.valueOf(moves_not_equal_dice_number));
		// System.out.println("No move on dice number: " +
		// String.valueOf(no_move_dice_number));
		System.out.println("Add piece to column 25: " + String.valueOf(piece_to_column_25));
		System.out.println("Moves unidentified: " + String.valueOf(different_number));
	}

	private void checkDiceToNumMoves(HashMap<Integer, ArrayList<List<Integer>>> states, HashMap<Integer, List<Integer>> dices) {
		for (Integer key : states.keySet()) {
			ArrayList<List<Integer>> state = states.get(key);
			if (key == 0) {
				continue;
			}
			if (dices.get(key).size() == state.size()) {
				moves_equal_dice_number++;
			} else {

				if (dices.get(key).size() * 2 == state.size()) {
					moves_double_dice_number++;
					if (dices.get(key).get(0) == dices.get(key).get(1)) {
						same_dice_on_double_move++;
					}
				} else if (state.size() == 0) {
					no_move_dice_number++;
				} else if (dices.get(key).size() + 1 == state.size()) {
					// last move has a extra piece on column 25
					if (state.size() > 2) {
						if (state.get(state.size() - 1).get(25) > state.get(state.size() - 2).get(25))
							piece_to_column_25++;
					}

				} else {
					different_number++;
				}
				moves_not_equal_dice_number++;
			}
		}
	}

	protected void determineNoMoveRule() {
		FastVector atts;
		Instances data;
		double[] vals;
		atts = new FastVector();

		for (int i = 0; i < matches.get(0).initial_state().length; i++) {
			atts.addElement(new Attribute("column_" + String.valueOf(i)));
		}

		FastVector has_moved = new FastVector();
		has_moved.addElement("yes");
		has_moved.addElement("no");

		atts.addElement(new Attribute("has_moved", has_moved));

		// 2. create Instances object
		data = new Instances("NoMoveStateRelation", atts, 0);

		// 3. fill with data
		for (Match m : this.matches) {
			HashMap<Integer, ArrayList<List<Integer>>> states = m.states(0);
			List<Integer> previous_move = null;

			for (Integer key : states.keySet()) {
				ArrayList<List<Integer>> state = states.get(key);
				if (key == 0) {
					continue;
				}

				if (state.size() == 0) {

					if (previous_move != null) {
						vals = new double[data.numAttributes()];
						for (int i = 0; i < previous_move.size(); i++) {
							vals[i] = (double) previous_move.get(i);
						}
						vals[data.numAttributes() - 1] = 1;
						data.add(new Instance(1.0, vals));
					}
				} else {

					for (List<Integer> move : state) {
						if (move.get(25) == 0) {
							vals = new double[data.numAttributes()];
							for (int i = 0; i < move.size(); i++) {
								vals[i] = (double) move.get(i);
							}
							vals[data.numAttributes() - 1] = 0;
							data.add(new Instance(1.0, vals));
						} else {
							previous_move = move;
						}
					}
				}

			}
			previous_move = null;
			states = m.states(1);
			for (Integer key : states.keySet()) {
				ArrayList<List<Integer>> state = states.get(key);
				if (key == 0) {
					continue;
				}

				if (state.size() == 0) {

					if (previous_move != null) {
						vals = new double[data.numAttributes()];
						for (int i = 0; i < previous_move.size(); i++) {
							vals[i] = (double) previous_move.get(i);
						}
						vals[data.numAttributes() - 1] = 1;
						data.add(new Instance(1.0, vals));
					}
				} else {

					for (List<Integer> move : state) {
						if (move.get(25) == 0) {
							vals = new double[data.numAttributes()];
							for (int i = 0; i < move.size(); i++) {
								vals[i] = (double) move.get(i);
							}
							vals[data.numAttributes() - 1] = 0;
							data.add(new Instance(1.0, vals));
						} else {
							previous_move = move;
						}
					}
				}
			}
		}
		data.setClassIndex(data.numAttributes() - 1);
		J48 tree = new J48();

		Evaluation eval;
		try {
			eval = new Evaluation(data);
			eval.crossValidateModel(tree, data, 5, new Random(1));
			System.out.println("Can move rule percent correct: " + Double.toString(eval.pctCorrect()));
			tree.buildClassifier(data);

			final javax.swing.JFrame jf = new javax.swing.JFrame("Can move rule");
			jf.setSize(500, 400);
			jf.getContentPane().setLayout(new BorderLayout());
			TreeVisualizer tv = new TreeVisualizer(null, tree.graph(), new PlaceNode2());
			jf.getContentPane().add(tv, BorderLayout.CENTER);
			jf.addWindowListener(new java.awt.event.WindowAdapter() {
				public void windowClosing(java.awt.event.WindowEvent e) {
					jf.dispose();
				}
			});

			jf.setVisible(true);
			tv.fitToScreen();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// special positions
	private void determineSpecialPositions() {
		System.out.println("========== SPECIAL POSITIONS =========");
		specialPositionsOut = new int[26];
		specialPositionsIn = new int[26];
		for (Match m : matches) {
			determineSpecialPosition(m.states(0));
		}
		printSpecialPositions(1);

		specialPositionsOut = new int[26];
		specialPositionsIn = new int[26];
		for (Match m : matches) {
			determineSpecialPosition(m.states(1));
		}
		printSpecialPositions(2);
	}

	protected void printSpecialPositions(int player) {
		System.out.print("OUT POSITIONS FOR PLAYER " + String.valueOf(player) + ": ");
		for (int i = 0; i < specialPositionsOut.length; i++) {
			if (specialPositionsOut[i] != 0) {
				System.out.print(i);
				System.out.print(" ");
			}
		}
		System.out.println();
		System.out.print("IN POSITIONS FOR PLAYER " + String.valueOf(player) + ": ");
		for (int i = 0; i < specialPositionsIn.length; i++) {
			if (specialPositionsIn[i] != 0) {
				System.out.print(i);
				System.out.print(" ");
			}
		}
		System.out.println();
	}

	private void determineSpecialPosition(HashMap<Integer, ArrayList<List<Integer>>> states) {
		int index_minus = 0, index_plus = 0;
		List<Integer> previous_move = null;

		for (Integer key : states.keySet()) {
			ArrayList<List<Integer>> state = states.get(key);
			for (List<Integer> move : state) {
				if (previous_move == null) {
					previous_move = move;
				} else {
					index_minus = index_plus = 0;
					// determine changed indexes
					for (int i = 1; i < move.size() - 1; i++) {
						if (previous_move.get(i) != move.get(i)) {
							if (previous_move.get(i) > move.get(i)) {
								index_minus = i;
							} else {
								index_plus = i;
							}
						}
					}
					// a piece was taken out of the board
					if (index_plus == 0 && index_minus != 0 && move.get(0) != previous_move.get(0)) {
						specialPositionsOut[index_minus]++;
					} else if (index_minus == 0 && index_plus != 0 && move.get(move.size() - 1) != previous_move.get(move.size() - 1)) {
						specialPositionsIn[index_plus]++;
					}
					previous_move = move;
				}
			}
		}
	}

	// moving direction
	protected void determineMovingDirection() {

		// 1 - large to small
		// 2 - small to large
		System.out.println("========== DIRECTION =========");
		small_to_large = 0;
		large_to_small = 0;
		undefined_direction = 0;
		total = 0;
		for (Match m : matches) {
			determinePlayerDirection(m.states(0));
		}
		// extract the data
		printPlayerDirection(1);
		small_to_large = 0;
		large_to_small = 0;
		undefined_direction = 0;
		total = 0;
		for (Match m : matches) {
			determinePlayerDirection(m.states(1));
		}
		// extract the data
		printPlayerDirection(2);
		System.out.println();
	}

	protected void printPlayerDirection(int player) {
		int total_moves = total - undefined_direction;
		// System.out.println("========== PLAYER " + String.valueOf(player) +
		// " ===========");
		// System.out.println("Total moves:" + String.valueOf(total));
		// System.out.println("Moves with NO direction:" +
		// String.valueOf(undefined_direction));
		//
		// System.out.print("Large index to small index movement:");
		// System.out.print(large_to_small * 100.0 / total_moves);
		// System.out.println(" %");
		//
		// System.out.print("Small index to large index movement:");
		// System.out.print(small_to_large * 100.0 / total_moves);
		// System.out.println(" %");
		if (large_to_small * 100.0 / total_moves > small_to_large * 100.0 / total_moves) {
			System.out.println("GENERAL DIRECTION FOR PLAYER " + String.valueOf(player) + " : Large index to small index");
		} else {
			System.out.println("GENERAL DIRECTION FOR PLAYER " + String.valueOf(player) + " : Small index to large index");
		}
	}

	protected void determinePlayerDirection(HashMap<Integer, ArrayList<List<Integer>>> states) {
		int index_minus = 0, index_plus = 0;
		List<Integer> previous_move = null;

		for (Integer key : states.keySet()) {
			ArrayList<List<Integer>> state = states.get(key);
			for (List<Integer> move : state) {
				if (previous_move == null) {
					previous_move = move;
				} else {
					index_minus = index_plus = 0;
					// determine changed indexes
					for (int i = 1; i < move.size() - 1; i++) {
						if (previous_move.get(i) != move.get(i)) {
							if (previous_move.get(i) > move.get(i)) {
								index_minus = i;
							} else {
								index_plus = i;
							}
						}
					}
					// determine direction
					if (index_minus == 0 || index_plus == 0) {
						undefined_direction++;
					} else {
						if (index_minus > index_plus) {
							large_to_small++;
						}
						if (index_minus < index_plus) {
							small_to_large++;
						}
					}
					total++;
					previous_move = move;
				}
			}
		}
	}
}
