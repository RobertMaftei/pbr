package pbr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Match {

	protected HashMap<Integer, ArrayList<List<Integer>>> states_p1 = new HashMap<Integer, ArrayList<List<Integer>>>();
	protected HashMap<Integer, ArrayList<List<Integer>>> states_p2 = new HashMap<Integer, ArrayList<List<Integer>>>();
	protected HashMap<Integer, List<Integer>> dices_p1 = new HashMap<Integer, List<Integer>>();
	protected HashMap<Integer, List<Integer>> dices_p2 = new HashMap<Integer, List<Integer>>();

	protected int winner = -1;

	public void setDices(int player, int round, List<Integer> dices) {
		HashMap<Integer, List<Integer>> states;
		if (player == 0) {
			states = dices_p1;
		} else {
			states = dices_p2;
		}
		states.put(round, dices);
	}
	
	public HashMap<Integer, List<Integer>> getDices(int player) {
		HashMap<Integer, List<Integer>> states;
		if (player == 0) {
			states = dices_p1;
		} else {
			states = dices_p2;
		}
		return states;
	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}

	public void init_round_state(int number) {
		ArrayList<List<Integer>> round = states_p1.getOrDefault(number, new ArrayList<List<Integer>>());
		states_p1.put(number, round);
		round = states_p2.getOrDefault(number, new ArrayList<List<Integer>>());
		states_p2.put(number, round);
		List<Integer> dices = dices_p1.getOrDefault(number, new ArrayList<Integer>());
		dices_p1.put(number, dices);
		dices = dices_p2.getOrDefault(number, new ArrayList<Integer>());
		dices_p2.put(number, dices);
	}

	public void add_state(int[] board_state, int number, int player) {
		HashMap<Integer, ArrayList<List<Integer>>> states;
		if (player == 0) {
			states = states_p1;
		} else {
			states = states_p2;
			// reverse direction for second palyer
			int[] rev_boar_state = new int[board_state.length];
			rev_boar_state[0] = board_state[0];
			rev_boar_state[board_state.length -1] = board_state[board_state.length - 1];
			
			for(int i = board_state.length - 2; i >= 1; i--)
			{
				rev_boar_state[board_state.length - i - 1] = board_state[i];
			}
		
			board_state = rev_boar_state;
		}
		ArrayList<List<Integer>> round = states.getOrDefault(number,
				new ArrayList<List<Integer>>());

		List<Integer> intList = new ArrayList<Integer>();
		for (int index = 0; index < board_state.length; index++) {
			intList.add(board_state[index]);
		}
		round.add(intList);
		states.put(number, round);
	}

	public int[] final_state(int player) {
		HashMap<Integer, ArrayList<List<Integer>>> states;
		if (player == 0) {
			states = states_p1;
		} else {
			states = states_p2;
		}
		ArrayList<List<Integer>> last_round = states
				.get(states.keySet().size() - 1);
		if (last_round.size() == 0) {
			last_round = states
					.get(states.keySet().size() - 2);
		}
		List<Integer> last_state = last_round.get(last_round.size() - 1);
		int[] int_last_state = new int[last_state.size()];
		for (int i = 0; i < last_state.size(); i++) {
			int_last_state[i] = last_state.get(i);
		}
		return int_last_state;
	}

	public HashMap<Integer, ArrayList<List<Integer>>> states(int player) {
		HashMap<Integer, ArrayList<List<Integer>>> states;
		if (player == 0) {
			states = states_p1;
		} else {
			states = states_p2;
		}
		return states;
	}

	public int[] initial_state() {
		return convertIntegers(states_p1.get(0).get(0));
	}

	public static int[] convertIntegers(List<Integer> integers) {
		int[] ret = new int[integers.size()];
		Iterator<Integer> iterator = integers.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next().intValue();
		}
		return ret;
	}
}
