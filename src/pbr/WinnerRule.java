package pbr;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Random;

public class WinnerRule {
	protected String arffFile = "winner_file.arff";
	protected List<Match> matches;

	public void printWinnerRule(List<Match> matches) throws Exception {
		this.matches = matches;
		Instances data = this.toArff();

		J48 tree = new J48();

		Evaluation eval = new Evaluation(data);
		eval.crossValidateModel(tree, data, 5, new Random(1));
		System.out.println("Winner rule percent correct: " +
				Double.toString(eval.pctCorrect()));
		tree.buildClassifier(data);

		final javax.swing.JFrame jf = new javax.swing.JFrame("Winner rule");
		jf.setSize(500, 400);
		jf.getContentPane().setLayout(new BorderLayout());
		TreeVisualizer tv = new TreeVisualizer(null, tree.graph(), new PlaceNode2());
		jf.getContentPane().add(tv, BorderLayout.CENTER);
		jf.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				jf.dispose();
			}
		});

		jf.setVisible(true);
		tv.fitToScreen();
	}

	protected Instances toArff() throws FileNotFoundException {
		FastVector atts;
		Instances data;
		double[] vals;

		atts = new FastVector();
		atts.addElement(new Attribute("player_1_bar"));
		atts.addElement(new Attribute("player_2_bar"));

		FastVector winner = new FastVector();
		winner.addElement("first_player");
		winner.addElement("second_player");

		atts.addElement(new Attribute("winner", winner));

		// 2. create Instances object
		data = new Instances("WinnerBarRelation", atts, 0);

		// 3. fill with data
		for (Match m : this.matches) {
			vals = new double[data.numAttributes()];
			vals[0] = m.final_state(0)[0];
			vals[1] = m.final_state(1)[0];
			vals[2] = m.getWinner();
			data.add(new Instance(1.0, vals));
		}
		data.setClassIndex(data.numAttributes() - 1);
		return data;
	}
}
