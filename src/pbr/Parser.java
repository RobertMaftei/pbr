package pbr;

import org.json.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Parser {
	protected String file;
	protected List<Match> matches = new ArrayList<Match>();
	private Scanner scanner;

	public Parser(String file) {
		super();
		this.file = file;
	}

	public List<Match> parse() {
		String content;
		try {
			scanner = new Scanner(new File(file));
			content = scanner.useDelimiter("\\Z").next();
			// System.out.println(content);
			String[] matches = content.split("\\r\\n\\r\\n");
			// System.out.println(matches.length);
			for (String match : matches) {
//				System.out.println();
				Match m = this.parse_match(match);
				this.matches.add(m);
				
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.matches;
	}

	protected Match parse_match(String match) {
		Match m = new Match();
		List<String[]> state = new ArrayList<>();
		List<JSONArray> parsed_match = new ArrayList<>();

		String[] rows = match.split("\\r\\n");

		rows = Arrays.copyOfRange(rows, 1, rows.length);
		rows = Arrays.copyOfRange(rows, 1, rows.length);
		rows = Arrays.copyOf(rows, rows.length - 1);
		for (int i = 0; i < rows.length; i++) {

			String[] moves = rows[i].trim().split("  ", 2);
			for (int j = 0; j < moves.length; j++) {
				moves[j] = moves[j].trim();
			}
			state.add(moves);
		}

		int round_number = 0;
		for (String[] round : state) {
			round_number++;
			String[] first_part = round[0].split("\\) ", 2);
			try {
				if (first_part.length > 1) {
					JSONObject move_1 = this.parse_move(round_number, 0,
							first_part[1]);
					parsed_match.add(move_1.getJSONArray("move"));
				}

				if (round.length > 1) {
					JSONObject move_2 = this.parse_move(round_number, 1,
							round[1]);
					parsed_match.add(move_2.getJSONArray("move"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		try {
			int[] s = getInitialState(parsed_match);
			m.add_state(s, 0, 0);
			m.add_state(s, 0, 1);
			set_all_states(parsed_match, m);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m;
	}

	private void set_all_states(List<JSONArray> match, Match m) throws JSONException {
		int[][] current_state = { m.initial_state(), m.initial_state() };
		int player = -1;
		for (JSONArray round : match) {
			player = round.getInt(1);
			int other_player = Math.abs(player - 1);
			m.init_round_state(round.getInt(0));
			
			ArrayList<Integer> dices = new ArrayList<Integer>();
			dices.add(round.getInt(2));
			dices.add(round.getInt(3));
			m.setDices(player, round.getInt(0), dices);
			
			for (int i = 4; i < round.length(); i++) {
				int from, to;
				boolean star;
				from = round.getJSONArray(i).getInt(0);
				to = round.getJSONArray(i).getInt(1);
				star = round.getJSONArray(i).getBoolean(2);
				if (current_state[player][from] == 0) {
					current_state[player][from]++;
				}
				if (star) {
					if (current_state[other_player][25 - to] == 0) {
						current_state[other_player][25 - to]++;
					}
					current_state[other_player][25 - to]--;
					current_state[other_player][25]++;
					m.add_state(current_state[other_player], round.getInt(0), other_player);
				}
				current_state[player][from]--;
				current_state[player][to]++;
				
				m.add_state(current_state[player], round.getInt(0), player);
			}
		}
		m.setWinner(player);
	}

	protected JSONObject parse_move(int round_number, int player, String move)
			throws JSONException {
		JSONObject move_container = new JSONObject();
		JSONArray final_move = new JSONArray();
		final_move.put(round_number);
		final_move.put(player);
		move_container.put("move", final_move);
		String[] moves = move.split(": ");
		final_move.put(Character.getNumericValue(moves[0].charAt(0)));
		final_move.put(Character.getNumericValue(moves[0].charAt(1)));
		if (moves.length > 1) {
			for (String piece_move : moves[1].split(" ")) {
				String[] piece_moves = piece_move.split("\\/", 2);
				boolean star = false;
				
				if (piece_moves[1].charAt(piece_moves[1].length() - 1) == '*') {
					star = true;
					piece_moves[1] = piece_moves[1].substring(0,
							piece_moves[1].length() - 1);
				}
				JSONArray m = new JSONArray();
				m.put(Integer.parseInt(piece_moves[0]));
				m.put(Integer.parseInt(piece_moves[1]));
				m.put(star);
				final_move.put(m);
			}
		}
		return move_container;
	}

	protected int[] getInitialState(List<JSONArray> match)
			throws JSONException {
		int[][] initial_state = { blankState(), blankState() };
		int[][] current_state = { blankState(), blankState() };

		for (JSONArray round : match) {
			int player = round.getInt(1);
			int other_player = Math.abs(player - 1);
			for (int i = 4; i < round.length(); i++) {
				int from, to;
				boolean star;
				from = round.getJSONArray(i).getInt(0);
				to = round.getJSONArray(i).getInt(1);
				star = round.getJSONArray(i).getBoolean(2);
				if (current_state[player][from] == 0) {
					initial_state[player][from]++;
					current_state[player][from]++;
				}
				if (star) {
					if (current_state[other_player][25 - to] == 0) {
						initial_state[other_player][25 - to]++;
						current_state[other_player][25 - to]++;
					}
					current_state[other_player][25 - to]--;
					current_state[other_player][25]++;
				}
				current_state[player][from]--;
				current_state[player][to]++;
			}
		}
		return initial_state[0];
	}

	protected int[] blankState() {
		return new int[26];
	}
}
