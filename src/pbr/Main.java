package pbr;

import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Parser parser = new Parser("matches.mat");
		List<Match> matches = parser.parse();

		printInitial(matches.get(0));
		WinnerRule w = new WinnerRule();
		w.printWinnerRule(matches);
		System.out.println("====================================");
		Rules r = new Rules();
		r.determine_rules(matches);
	}

	public static void printInitial(Match m) {
		System.out.println("Initial state:");
		int[] state = m.initial_state();
		for (int i = 1; i < 7; i++) {
			System.out.print(state[i]);
			System.out.print(" ");
		}
		System.out.print("|| ");
		for (int i = 7; i < 13; i++) {
			System.out.print(state[i]);
			System.out.print(" ");
		}
		System.out.println();
		for (int i = 0; i < 6; i++) {
			System.out.print("| ");
		}
		System.out.print("|| ");
		for (int i = 7; i < 13; i++) {
			System.out.print("| ");
		}
		System.out.println();
		for (int i = 24; i > 18; i--) {
			System.out.print(state[i]);
			System.out.print(" ");
		}
		System.out.print("|| ");
		for (int i = 18; i > 12; i--) {
			System.out.print(state[i]);
			System.out.print(" ");
		}
		System.out.println();
		System.out.println("==========================");
	}

}
